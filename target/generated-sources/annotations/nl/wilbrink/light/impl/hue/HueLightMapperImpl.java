package nl.wilbrink.light.impl.hue;

import javax.annotation.Generated;
import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.dto.LightStateDTO;
import nl.wilbrink.light.impl.hue.LightResponse.State;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-02-11T19:40:30+0100",
    comments = "version: 1.2.0.CR2, compiler: javac, environment: Java 1.8.0_144 (Oracle Corporation)"
)
@Component
public class HueLightMapperImpl implements HueLightMapper {

    @Override
    public LightDTO toDTO(LightResponse entity) {
        if ( entity == null ) {
            return null;
        }

        LightDTO lightDTO = new LightDTO();

        lightDTO.setState( stateToLightStateDTO( entity.getState() ) );
        lightDTO.setName( entity.getName() );

        return lightDTO;
    }

    protected LightStateDTO stateToLightStateDTO(State state) {
        if ( state == null ) {
            return null;
        }

        LightStateDTO lightStateDTO = new LightStateDTO();

        if ( state.getReachable() != null ) {
            lightStateDTO.setReachable( state.getReachable() );
        }
        if ( state.getOn() != null ) {
            lightStateDTO.setOn( state.getOn() );
        }
        lightStateDTO.setHue( (long) state.getHue() );
        lightStateDTO.setSaturation( (long) state.getSaturation() );
        lightStateDTO.setBrightness( (long) state.getBrightness() );
        if ( state.getTransitionTime() != null ) {
            lightStateDTO.setTransitionTime( state.getTransitionTime().longValue() );
        }

        return lightStateDTO;
    }
}
