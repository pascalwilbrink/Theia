package nl.wilbrink.config.mapper;

import javax.annotation.Generated;
import nl.wilbrink.config.ConfigItemKey;
import nl.wilbrink.config.dto.ConfigItemDTO;
import nl.wilbrink.config.entity.ConfigItem;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2018-02-11T19:40:29+0100",
    comments = "version: 1.2.0.CR2, compiler: javac, environment: Java 1.8.0_144 (Oracle Corporation)"
)
@Component
public class ConfigItemMapperImpl implements ConfigItemMapper {

    @Override
    public ConfigItemDTO toDTO(ConfigItem entity) {
        if ( entity == null ) {
            return null;
        }

        ConfigItemDTO configItemDTO = new ConfigItemDTO();

        configItemDTO.setId( entity.getId() );
        if ( entity.getKey() != null ) {
            configItemDTO.setKey( Enum.valueOf( ConfigItemKey.class, entity.getKey() ) );
        }
        configItemDTO.setValue( entity.getValue() );

        return configItemDTO;
    }

    @Override
    public ConfigItem toEntity(ConfigItemDTO dto) {
        if ( dto == null ) {
            return null;
        }

        ConfigItem configItem = new ConfigItem();

        configItem.setId( dto.getId() );
        if ( dto.getKey() != null ) {
            configItem.setKey( dto.getKey().name() );
        }
        configItem.setValue( dto.getValue() );

        return configItem;
    }
}
