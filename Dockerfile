FROM frolvlad/alpine-oraclejdk8:slim
EXPOSE 1357
ADD target/theia-0.0.1-SNAPSHOT.jar /theia.jar
ENTRYPOINT ["java","-Dspring.profiles.active=docker","-jar","/theia.jar"]
