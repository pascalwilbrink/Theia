package nl.wilbrink;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rollbar.notifier.Rollbar;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.hibernate.dialect.H2Dialect;
import org.hibernate.dialect.PostgreSQL9Dialect;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.sql.DataSource;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.rollbar.notifier.config.ConfigBuilder.withAccessToken;

@Configuration
@ComponentScan(
    basePackages = {"nl.wilbrink"}
)
@EnableJpaRepositories
@EnableJpaAuditing
public class ApplicationConfig {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private Environment env;

    private Logger logger = Logger.getGlobal();

    @Bean
    ObjectMapper objectMapper() {
        final ObjectMapper mapper = new ObjectMapper();

        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
        mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, false);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        return mapper;
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry
                        .addMapping("/**")
                        .allowedMethods("GET", "PUT", "POST", "DELETE", "OPTIONS");
            }
        };
    }

    @Bean
    LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManager = new LocalContainerEntityManagerFactoryBean();

        entityManager.setDataSource(dataSource);
        entityManager.setPackagesToScan("nl.wilbrink");
        entityManager.setJpaVendorAdapter(jpaVendorAdapter());
        entityManager.setPersistenceProviderClass(HibernatePersistenceProvider.class);

        return entityManager;
    }

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setShowSql(true);

        if (determineDatabase().equals(Database.H2)) {
            jpaVendorAdapter.setDatabase(Database.H2);
            jpaVendorAdapter.setDatabasePlatform(H2Dialect.class.getName());
        } else if (determineDatabase().equals(Database.POSTGRESQL)) {
            jpaVendorAdapter.setDatabase(Database.POSTGRESQL);
            jpaVendorAdapter.setDatabasePlatform(PostgreSQL9Dialect.class.getName());
        }

        jpaVendorAdapter.setGenerateDdl(true);

        return jpaVendorAdapter;
    }

    @Bean
    Flyway flyway() {
        Flyway flyway = new Flyway();

        flyway.setDataSource(dataSource);

        final String[] locations = this.getFlywayLocations();

        if (locations.length > 0) {
            flyway.setLocations(locations);

            try {
                flyway.migrate();
            } catch (FlywayException e) {
                logger.log(Level.SEVERE, String.format("Flyway error: %s", e.getMessage()));
            }
        }

        return flyway;
    }

    @Bean
    Rollbar rollbar() {
        return Rollbar.init(withAccessToken(env.getProperty("rollbar.api")).environment("development").build());
    }

    private Database determineDatabase() {
        Database database;

        String db = env.getProperty("database", "H2").toUpperCase();
        if (db.equals("H2")) {
            database = Database.H2;
        } else if (db.equals("POSTGRESQL")) {
            database = Database.POSTGRESQL;
        } else {
            database = Database.H2;
        }

        return database;
    }

    private String[] getFlywayLocations() {
        switch (determineDatabase()) {
            case H2:
                return new String[]{"migration/h2"};
            case POSTGRESQL:
                return new String[]{"migration/postgres"};
            default:
                return new String[]{};
        }
    }

}