package nl.wilbrink;

import nl.wilbrink.common.websocket.BaseWebSocketSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import java.util.List;

@Configuration
@EnableWebSocket
public class WebsocketConfig implements WebSocketConfigurer {

    private final List<? extends BaseWebSocketSender> websocketHandlers;

    @Autowired
    public WebsocketConfig(List<? extends BaseWebSocketSender> websocketHandlers) {
        this.websocketHandlers = websocketHandlers;
    }

    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        websocketHandlers.forEach(websocketHandler -> {
            registry.addHandler(websocketHandler, websocketHandler.getEndpoint()).setAllowedOrigins(websocketHandler.getAllowedOrigins());
        });
    }

}