package nl.wilbrink.config.mapper;

import nl.wilbrink.config.dto.ConfigItemDTO;
import nl.wilbrink.config.entity.ConfigItem;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ConfigItemMapper {

    @Mapping(target = "possibleValues", ignore = true)
    ConfigItemDTO toDTO(ConfigItem entity);

    ConfigItem toEntity(ConfigItemDTO dto);

}
