package nl.wilbrink.config.dto;

import nl.wilbrink.config.ConfigItemKey;

import java.util.ArrayList;
import java.util.List;

public class ConfigItemDTO {

    private String id;

    private ConfigItemKey key;

    private String value;

    private List<String> possibleValues = new ArrayList<>();

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setKey(ConfigItemKey key) {
        this.key = key;
    }

    public ConfigItemKey getKey() {
        return key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void addPossibleValue(String value) {
        this.possibleValues.add(value);
    }

    public List<String> getPossibleValues() {
        return possibleValues;
    }
}
