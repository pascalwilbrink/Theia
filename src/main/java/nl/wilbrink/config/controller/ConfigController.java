package nl.wilbrink.config.controller;

import nl.wilbrink.config.dto.ConfigItemDTO;
import nl.wilbrink.config.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/config")
public class ConfigController {

    private final ConfigService configService;

    @Autowired
    public ConfigController(ConfigService configService) {
        this.configService = configService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllConfigItems() {
        List<ConfigItemDTO> config = configService.getAllConfigItems();

        return ResponseEntity.ok(config);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getConfigItem(@PathVariable("id") String id) {
        ConfigItemDTO configItem = configService.getConfigItemById(id);

        return ResponseEntity.ok(configItem);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity updateConfigItem(@PathVariable("id") String id, @RequestBody  ConfigItemDTO configItem) {
        configItem.setId(id);

        configItem = configService.updateConfigItem(configItem);

        return ResponseEntity.ok(configItem);
    }
}
