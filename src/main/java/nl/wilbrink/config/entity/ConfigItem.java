package nl.wilbrink.config.entity;

import nl.wilbrink.common.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "config_items")
public class ConfigItem extends BaseEntity {

    @Column(name = "config_key")
    private String key;

    @Column(name = "config_value")
    private String value;

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
