package nl.wilbrink.config.repository;

import nl.wilbrink.config.entity.ConfigItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConfigItemRepository extends CrudRepository<ConfigItem, String> {

    ConfigItem findOneByKey(String key);
}
