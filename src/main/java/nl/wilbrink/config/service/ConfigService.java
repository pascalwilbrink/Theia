package nl.wilbrink.config.service;

import nl.wilbrink.common.exception.WebException;
import nl.wilbrink.config.ConfigItemKey;
import nl.wilbrink.config.dto.ConfigItemDTO;
import nl.wilbrink.config.entity.ConfigItem;
import nl.wilbrink.config.mapper.ConfigItemMapper;
import nl.wilbrink.config.repository.ConfigItemRepository;
import nl.wilbrink.light.LightType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static nl.wilbrink.common.exception.WebException.HttpStatus.BAD_REQUEST;

@Service
public class ConfigService {

    private final ConfigItemRepository configItemRepository;
    private final ConfigItemMapper configItemMapper;

    private Map<ConfigItemKey, PossibleValueMapper> valueMappers = new HashMap<>();

    @Autowired
    public ConfigService(ConfigItemRepository configItemRepository, ConfigItemMapper configItemMapper) {
        this.configItemRepository = configItemRepository;
        this.configItemMapper = configItemMapper;

        createValueMappers();
    }

    public List<ConfigItemDTO> getAllConfigItems() {
        List<ConfigItemDTO> configItems = new ArrayList<>();

        configItemRepository.findAll().forEach(entity -> {
            ConfigItemDTO configItem = configItemMapper.toDTO(entity);
            addPossibleValues(configItem);

            configItems.add(configItem);
        });

        return configItems;
    }

    public ConfigItemDTO getConfigItemById(String id) {
        return configItemMapper.toDTO(configItemRepository.findOne(id));
    }

    public ConfigItemDTO getConfigItemByKey(ConfigItemKey key) {
        return configItemMapper.toDTO(configItemRepository.findOneByKey(key.name()));
    }

    public ConfigItemDTO updateConfigItem(ConfigItemDTO configItem) {
        ConfigItem entity = configItemMapper.toEntity(configItem);

        if (entity.isNew()) {
            throw new WebException("Cannot update config item without id", BAD_REQUEST);
        }

        entity = configItemRepository.save(entity);

        return configItemMapper.toDTO(entity);
    }

    private void addPossibleValues(ConfigItemDTO configItem) {
        if (valueMappers.containsKey(configItem.getKey())) {
            valueMappers.get(configItem.getKey()).addValues(configItem);
        }
    }

    private void createValueMappers() {
        valueMappers.put(ConfigItemKey.LIGHT_TYPE, configItem -> {
            Arrays.asList(LightType.values()).forEach(lightType -> configItem.addPossibleValue(lightType.name()));
        });
    }

    private interface PossibleValueMapper {
        void addValues(ConfigItemDTO configItem);
    }
}
