package nl.wilbrink.light.websocket;

import nl.wilbrink.common.websocket.BaseWebSocketSender;
import org.springframework.stereotype.Component;

@Component
public class LightWebsocketSender extends BaseWebSocketSender {

    @Override
    public String getEndpoint() {
        return "/ws";
    }

}