package nl.wilbrink.light.websocket;

import nl.wilbrink.common.websocket.BaseWebSocketSender;

public class ServerLightWebSocketSender extends BaseWebSocketSender {

    @Override
    public String getEndpoint() {
        return "/event/ws";
    }
}
