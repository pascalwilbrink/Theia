package nl.wilbrink.light.websocket;

public class SwitchLightEvent {

    private final Boolean value;

    private final String lightId;

    public SwitchLightEvent(String lightId, Boolean value) {
        this.lightId = lightId;
        this.value = value;
    }

    public Boolean getValue() {
        return value;
    }

    public String getLightId() {
        return lightId;
    }
}
