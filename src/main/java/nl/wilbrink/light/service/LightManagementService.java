package nl.wilbrink.light.service;

import nl.wilbrink.config.service.ConfigService;
import nl.wilbrink.light.LightProviderFactory;
import nl.wilbrink.light.LightType;
import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.dto.LightStateDTO;
import nl.wilbrink.light.utils.LightUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static nl.wilbrink.config.ConfigItemKey.LIGHT_TYPE;

@Service
public class LightManagementService {

    private final LightProviderFactory lightProviderFactory;
    private final ConfigService configService;
    private final LightUtils lightUtils;
    private final UpdateLightService updateLightService;

    @Autowired
    public LightManagementService(LightProviderFactory lightProviderFactory, ConfigService configService, LightUtils lightUtils,
            UpdateLightService updateLightService) {
        this.lightProviderFactory = lightProviderFactory;
        this.configService = configService;
        this.lightUtils = lightUtils;
        this.updateLightService = updateLightService;
    }

    public List<LightDTO> getAllLights() {
        return lightProviderFactory.getService(getLightType()).getAllLights();
    }

    public LightDTO getLight(String id) {
        return lightProviderFactory.getService(getLightType()).getLight(id);
    }

    public LightDTO updateLight(LightDTO light) {
        LightDTO oldLight = lightProviderFactory.getService(getLightType()).getLight(light.getId());

        if (!lightUtils.hasLightChange(oldLight, light)) {
            return oldLight;
        }

        LightDTO updatedLight = lightProviderFactory.getService(getLightType()).updateLight(light);
        updateLightService.handleLightIfChanged(oldLight, updatedLight);

        return updatedLight;
    }

    public LightStateDTO updateLightState(String id, LightStateDTO state) {
        LightStateDTO oldState = lightProviderFactory.getService(getLightType()).getLight(id).getState();

        if (!lightUtils.hasStateChange(oldState, state)) {
            return state;
        }

        LightStateDTO updatedState = lightProviderFactory.getService(getLightType()).updateLightState(id, state);
        updateLightService.handleLightStateIfChanged(oldState, updatedState, id);

        return updatedState;
    }

    private LightType getLightType() {
        return LightType.valueOf(configService.getConfigItemByKey(LIGHT_TYPE).getValue());
    }

}