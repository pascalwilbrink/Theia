package nl.wilbrink.light.service;

import nl.wilbrink.common.websocket.CRUDMessage;
import nl.wilbrink.common.websocket.WebSocketMessage;
import nl.wilbrink.light.audit.LightAuditProvider;
import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.dto.LightStateDTO;
import nl.wilbrink.light.utils.LightUtils;
import nl.wilbrink.light.websocket.LightWebsocketSender;
import nl.wilbrink.light.websocket.SwitchLightEvent;
import org.springframework.stereotype.Service;

import static nl.wilbrink.common.websocket.CRUDMessage.Method.UPDATE;

@Service
public class UpdateLightService {

    private final LightUtils lightUtils;
    private final LightAuditProvider lightAuditProvider;
    private final LightWebsocketSender lightWebsocketSender;

    public UpdateLightService(LightUtils lightUtils, LightAuditProvider lightAuditProvider, LightWebsocketSender lightWebsocketSender) {
        this.lightUtils = lightUtils;
        this.lightAuditProvider = lightAuditProvider;
        this.lightWebsocketSender = lightWebsocketSender;
    }

    public void handleLightStateIfChanged(LightStateDTO oldState, LightStateDTO newState, String id) {
        if (lightUtils.hasStateChange(oldState, newState)) {
            lightWebsocketSender.sendMessage(new CRUDMessage("Light", UPDATE));
            lightAuditProvider.auditEvent(oldState, newState);

            sendUpdateEvent(oldState, newState, id);
        }
    }

    public void handleLightIfChanged(LightDTO oldLight, LightDTO newLight) {
        if (lightUtils.hasLightChange(oldLight, newLight)) {
            lightWebsocketSender.sendMessage(new CRUDMessage("Light", UPDATE));
            lightAuditProvider.auditEvent(oldLight, newLight);
        }
    }

    private void sendUpdateEvent(LightStateDTO oldState, LightStateDTO updatedState, String lightId) {
        if (lightUtils.hasOnChange(oldState, updatedState)) lightWebsocketSender.sendMessage(new WebSocketMessage(new SwitchLightEvent(lightId, updatedState.getOn())));
        if (lightUtils.hasReachableChange(oldState, updatedState)) lightWebsocketSender.sendMessage(new WebSocketMessage(new SwitchLightEvent(lightId, updatedState.getOn())));
    }

}
