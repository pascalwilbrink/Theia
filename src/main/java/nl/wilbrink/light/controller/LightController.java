package nl.wilbrink.light.controller;

import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.dto.LightStateDTO;
import nl.wilbrink.light.service.LightManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/lights")
public class LightController {

    private final LightManagementService lightManagementService;

    @Autowired
    public LightController(LightManagementService lightManagementService) {
        this.lightManagementService = lightManagementService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllLights() {
        List<LightDTO> lights = lightManagementService.getAllLights();

        return ResponseEntity.ok(lights);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity getLight(@PathVariable("id") String id) {
        LightDTO light = lightManagementService.getLight(id);

        return ResponseEntity.ok(light);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity updateLight(@PathVariable("id") String id, @RequestBody LightDTO light) {
        light.setId(id);

        light = lightManagementService.updateLight(light);

        return ResponseEntity.ok(light);
    }

    @RequestMapping(value = "/{id}/state", method = RequestMethod.PUT)
    public ResponseEntity updateLightState(@PathVariable("id") String id, @RequestBody LightStateDTO state) {
        state = lightManagementService.updateLightState(id, state);

        return ResponseEntity.ok(state);
    }

}
