package nl.wilbrink.light;

import nl.wilbrink.common.exception.WebException;
import nl.wilbrink.common.logging.LoggingService;
import nl.wilbrink.light.impl.dummy.DummyLightService;
import nl.wilbrink.light.impl.hue.HueLightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static nl.wilbrink.common.exception.WebException.HttpStatus.BAD_REQUEST;

@Component
public class LightProviderFactory {

    private final LoggingService loggingService;

    private Map<LightType, LightService> services = new HashMap<>();

    @Autowired
    public LightProviderFactory(List<LightService> lightServices, LoggingService loggingService) {
        this.loggingService = loggingService;

        for(LightService lightService : lightServices) {
            this.services.put(lightService.getLightType(), lightService);
        }

    }

    public LightService getService(LightType type) {
        if(!this.services.containsKey(type)) {
            throw loggingService.error(String.format("Light provider with type [%s] not implemented", type.name()), new WebException(String.format("Light provider with type %s not found", type.name()), BAD_REQUEST));
        }

        return this.services.get(type);
    }

}
