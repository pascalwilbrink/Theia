package nl.wilbrink.light.impl.hue;

import nl.wilbrink.light.dto.LightDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface HueLightMapper {


    @Mapping(target = "state.on", source = "state.on")
    @Mapping(target = "state.reachable", source = "state.reachable")
    LightDTO toDTO(LightResponse entity);

}


