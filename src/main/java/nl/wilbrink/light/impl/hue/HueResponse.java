package nl.wilbrink.light.impl.hue;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;

import java.util.HashMap;
import java.util.Map;

public class HueResponse {

    private Success success;

    private Error error;

    public void setSuccess(Success success) {
        this.success = success;
    }

    public Success getSuccess() {
        return success;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public Error getError() {
        return error;
    }

    public static class Success {

        private Map<String, Object> other = new HashMap<>();

        @JsonAnySetter
        public void setParameter(String key, Object value) {
            this.other.put(key, value);
        }

        @JsonAnyGetter
        public Map<String, Object> getParameters() {
            return other;
        }

    }

    public static class Error {

        private Map<String, Object> other = new HashMap<>();

        @JsonAnySetter
        public void setParameter(String key, Object value) {
            this.other.put(key, value);
        }

        @JsonAnyGetter
        public Map<String, Object> getParameters() {
            return other;
        }

    }

}
