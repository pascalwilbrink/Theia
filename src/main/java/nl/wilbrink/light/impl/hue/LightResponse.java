package nl.wilbrink.light.impl.hue;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LightResponse {

    private State state;

    @JsonProperty("swupdate")
    private SwUpdate swUpdate;

    private Capabilities capabilities;

    private String type;

    private String name;

    @JsonProperty("modelid")
    private String modelId;

    @JsonProperty("uniqueid")
    private String uniqueId;

    @JsonProperty("swversion")
    private String swVersion;

    @JsonProperty("manufacturername")
    private String manufacturerName;

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }

    public void setSwUpdate(SwUpdate swUpdate) {
        this.swUpdate = swUpdate;
    }

    public SwUpdate getSwUpdate() {
        return swUpdate;
    }

    public void setCapabilities(Capabilities capabilities) {
        this.capabilities = capabilities;
    }

    public Capabilities getCapabilities() {
        return capabilities;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setSwVersion(String swVersion) {
        this.swVersion = swVersion;
    }

    public String getSwVersion() {
        return swVersion;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class State {

        private Boolean on;

        @JsonProperty("bri")
        private int brightness;

        private int hue;

        @JsonProperty("sat")
        private int saturation;

        private String effect;

        private float[] xy;

        @JsonProperty("ct")
        private String colorTemperature;

        private String alert;

        @JsonProperty("colormode")
        private String colorMode;

        private String mode;

        private Boolean reachable;

        @JsonProperty("transitiontime")
        private Integer transitionTime;

        public void setOn(boolean on) {
            this.on = on;
        }

        public Boolean getOn() {
            return on;
        }

        public void setBrightness(int brightness) {
            this.brightness = brightness;
        }

        public int getBrightness() {
            return brightness;
        }

        public void setHue(int hue) {
            this.hue = hue;
        }

        public int getHue() {
            return hue;
        }

        public void setSaturation(int saturation) {
            this.saturation = saturation;
        }

        public int getSaturation() {
            return saturation;
        }

        public void setEffect(String effect) {
            this.effect = effect;
        }

        public String getEffect() {
            return effect;
        }

        public void setXy(float[] xy) {
            this.xy = xy;
        }

        public float[] getXy() {
            return xy;
        }

        public void setColorTemperature(String colorTemperature) {
            this.colorTemperature = colorTemperature;
        }

        public String getColorTemperature() {
            return colorTemperature;
        }

        public void setAlert(String alert) {
            this.alert = alert;
        }

        public String getAlert() {
            return alert;
        }

        public void setColorMode(String colorMode) {
            this.colorMode = colorMode;
        }

        public String getColorMode() {
            return colorMode;
        }

        public void setMode(String mode) {
            this.mode = mode;
        }

        public String getMode() {
            return mode;
        }

        public void setReachable(boolean reachable) {
            this.reachable = reachable;
        }

        public Boolean getReachable() {
            return reachable;
        }

        public void setTransitionTime(Integer transitionTime) {
            this.transitionTime = transitionTime;
        }

        public Integer getTransitionTime() {
            return transitionTime;
        }
    }

    public static class SwUpdate {

        private String state;

        @JsonProperty("lastinstall")
        private String lastInstall;

        public void setState(String state) {
            this.state = state;
        }

        public String getState() {
            return state;
        }

        public void setLastInstall(String lastInstall) {
            this.lastInstall = lastInstall;
        }

        public String getLastInstall() {
            return lastInstall;
        }

    }

    public static class Capabilities {

        private boolean certified;

        private Streaming streaming;

        public void setCertified(boolean certified) {
            this.certified = certified;
        }

        public boolean getCertified() {
            return certified;
        }

        public void setStreaming(Streaming streaming) {
            this.streaming = streaming;
        }

        public Streaming getStreaming() {
            return streaming;
        }

        public static class Streaming {

            private Boolean renderer;

            private Boolean proxy;

            public void setRenderer(boolean renderer) {
                this.renderer = renderer;
            }

            public Boolean getRenderer() {
                return renderer;
            }

            public void setProxy(boolean proxy) {
                this.proxy = proxy;
            }

            public Boolean getProxy() {
                return proxy;
            }

        }

    }

}