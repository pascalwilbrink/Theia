package nl.wilbrink.light.impl.hue;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class LightsResponse {

    private Map<String, LightResponse> lights = new HashMap<>();

    @JsonAnySetter
    public void setLight(String id, LightResponse light) {
        this.lights.put(id, light);
    }

    @JsonAnyGetter
    public Map<String, LightResponse> getLights() {
        return lights;
    }

    public LightResponse getLight(String id) {
        return lights.get(id);
    }

}