package nl.wilbrink.light.impl.hue;

import feign.hystrix.FallbackFactory;
import nl.wilbrink.common.exception.WebException;
import nl.wilbrink.common.logging.LoggingService;
import nl.wilbrink.light.LightService;
import nl.wilbrink.light.LightType;
import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.dto.LightStateDTO;
import nl.wilbrink.light.entity.Light;
import nl.wilbrink.light.repository.LightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static nl.wilbrink.common.exception.WebException.HttpStatus.*;
import static nl.wilbrink.light.LightType.HUE;

@Service
public class HueLightService implements LightService {

    private final LightRepository lightRepository;
    private final HueAPIService hueAPIService;
    private final HueLightMapper lightMapper;
    private final LoggingService loggingService;

    @Autowired
    public HueLightService(LightRepository lightRepository, HueAPIService hueAPIService, HueLightMapper lightMapper, LoggingService loggingService) {
        this.lightRepository = lightRepository;
        this.hueAPIService = hueAPIService;
        this.lightMapper = lightMapper;
        this.loggingService = loggingService;
    }

    @Override
    public List<LightDTO> getAllLights() {
        List<LightDTO> lights = new ArrayList<>();

        LightsResponse lightsResponse = hueAPIService.getLights();

        lightsResponse.getLights().forEach((id, hueLight) -> {
            Light entity = lightRepository.findOneByExternalId(id);

            if (entity == null) {
                entity = new Light();
                entity.setExternalId(id);

                entity = lightRepository.save(entity);
            }

            LightDTO light = lightMapper.toDTO(hueLight);
            light.setId(entity.getId());
            lights.add(light);
        });

        return lights;
    }

    @Override
    public LightDTO getLight(String id) {
        Light entity = lightRepository.findOne(id);

        if (entity == null) {
            throw loggingService.error(String.format("Light with id [%s] not found", id), new WebException("Light not found", NOT_FOUND));
        }

        LightResponse response = hueAPIService.getLight(entity.getExternalId());

        LightDTO light = lightMapper.toDTO(response);
        light.setId(id);

        return light;
    }

    @Override
    public LightDTO updateLight(LightDTO light) {
        return light;
    }

    @Override
    public LightStateDTO updateLightState(String id, LightStateDTO state) {
        Light light = lightRepository.findOne(id);
        LightResponse.State hueState = new LightResponse.State();
        hueState.setOn(state.getOn());

        if (state.getBrightness() != null) {
            hueState.setBrightness(state.getBrightness().intValue());
        }
        if (state.getSaturation() != null) {
           hueState.setSaturation(state.getSaturation().intValue());
        }
        if (state.getHue() != null) {
            hueState.setHue(state.getHue().intValue());
        }
        if (state.getTransitionTime() != null) {
            hueState.setTransitionTime(state.getTransitionTime().intValue());
        }

        List<HueResponse> responses = hueAPIService.updateLightState(light.getExternalId(), hueState);

        if (!responses.stream().filter(response -> response.getError() != null)
                .collect(Collectors.toList()).isEmpty()) {
            throw loggingService.error(String.format("Error while updating hue light state %s", id), new WebException("Cannot update hue light state", BAD_REQUEST));
        }

        return getLight(id).getState();
    }

    @Override
    public LightType getLightType() {
        return HUE;
    }

    @FeignClient(
            url = "${hue.url}/api/${hue.api.key}",
            name = "Hue-api-service",
            fallbackFactory =  HueLightService.HueAPIFallbackFactory.class
    )
    public interface HueAPIService {

        @RequestMapping(
                value = "/lights",
                method = RequestMethod.GET
        )
        LightsResponse getLights();

        @RequestMapping(
                value = "/lights/{id}",
                method = RequestMethod.GET
        )
        LightResponse getLight(@PathVariable("id") String id);

        @RequestMapping(
                value = "/lights/{id}/state",
                method = RequestMethod.PUT
        )
        List<HueResponse> updateLightState(@PathVariable("id") String id, @RequestBody LightResponse.State state);

    }

    @Component
    public class HueAPIFallbackFactory implements FallbackFactory<HueAPIService> {

        @Override
        public HueAPIService create(Throwable cause) {
            throw loggingService.error("Error", new WebException("Cannot connect to Hue", INTERNAL_SERVER_ERROR, cause));
        }

    }

}
