package nl.wilbrink.light.impl.dummy;

import nl.wilbrink.light.LightService;
import nl.wilbrink.light.LightType;
import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.dto.LightStateDTO;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

import static nl.wilbrink.light.LightType.NONE;

@Service
public class DummyLightService implements LightService {

    @Override
    public List<LightDTO> getAllLights() {
        return Collections.emptyList();
    }

    @Override
    public LightDTO getLight(String id) {
        return null;
    }

    @Override
    public LightDTO updateLight(LightDTO light) {
        return null;
    }

    @Override
    public LightStateDTO updateLightState(String id, LightStateDTO state) {
        return null;
    }

    @Override
    public LightType getLightType() {
        return NONE;
    }
}
