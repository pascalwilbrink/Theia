package nl.wilbrink.light.entity;

import nl.wilbrink.common.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "lights")
public class Light extends BaseEntity {

    @Column(name = "external_id")
    private String externalId;

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getExternalId() {
        return externalId;
    }

}
