package nl.wilbrink.light;

import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.dto.LightStateDTO;

import java.util.List;

public interface LightService {

    List<LightDTO> getAllLights();

    LightDTO getLight(String id);

    LightDTO updateLight(LightDTO light);

    LightStateDTO updateLightState(String id, LightStateDTO state);

    LightType getLightType();

}
