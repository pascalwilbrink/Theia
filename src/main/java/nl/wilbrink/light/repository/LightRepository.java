package nl.wilbrink.light.repository;

import nl.wilbrink.light.entity.Light;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LightRepository extends CrudRepository<Light, String> {

    Light findOneByExternalId(String externalId);

}
