package nl.wilbrink.light.utils;

import java.util.Objects;

import org.springframework.stereotype.Component;

import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.dto.LightStateDTO;

@Component
public class LightUtils {

    public boolean hasLightChange(LightDTO oldLight, LightDTO newLight) {
        if (oldLight == newLight) return false;

        boolean hasLightChange = false;

        if (hasNameChange(oldLight, newLight)) hasLightChange = true;

        return hasLightChange;
    }

    public boolean hasStateChange(LightStateDTO oldState, LightStateDTO newState) {
        if (oldState == newState) return false;

        boolean hasStateChange = false;

        if (hasOnChange(oldState, newState)) hasStateChange = true;
        if (hasReachableChange(oldState, newState)) hasStateChange = true;
        if (hasHueChange(oldState, newState)) hasStateChange = true;
        if (hasSaturationChange(oldState, newState)) hasStateChange = true;
        if (hasBrightnessChange(oldState, newState)) hasStateChange = true;

        return hasStateChange;
    }

    public boolean hasNameChange(LightDTO oldLight, LightDTO newLight) {
        return !Objects.equals(oldLight.getName(), newLight.getName());
    }

    public boolean hasOnChange(LightStateDTO oldState, LightStateDTO newState) {
        return oldState.getOn() != newState.getOn();
    }

    public boolean hasReachableChange(LightStateDTO oldState, LightStateDTO newState) {
        return oldState.getReachable() != newState.getReachable();
    }

    public boolean hasHueChange(LightStateDTO oldState, LightStateDTO newState) {
        return !Objects.equals(oldState.getHue(), newState.getHue());
    }

    public boolean hasSaturationChange(LightStateDTO oldState, LightStateDTO newState) {
        return !Objects.equals(oldState.getSaturation(), newState.getSaturation());
    }

    public boolean hasBrightnessChange(LightStateDTO oldState, LightStateDTO newState) {
        return !Objects.equals(oldState.getBrightness(), newState.getBrightness());
    }

}
