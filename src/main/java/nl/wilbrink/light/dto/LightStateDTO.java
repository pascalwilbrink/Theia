package nl.wilbrink.light.dto;

public class LightStateDTO {

    private boolean on;

    private boolean reachable;

    private Long hue;

    private Long saturation;

    private Long brightness;

    private Long transitionTime;

    public void setOn(boolean on) {
        this.on = on;
    }

    public boolean getOn() {
        return on;
    }

    public void setReachable(boolean reachable) {
        this.reachable = reachable;
    }

    public boolean getReachable() {
        return reachable;
    }

    public void setHue(Long hue) {
        this.hue = hue;
    }

    public Long getHue() {
        return hue;
    }

    public void setSaturation(Long saturation) {
        this.saturation = saturation;
    }

    public Long getSaturation() {
        return saturation;
    }

    public void setBrightness(Long brightness) {
        this.brightness = brightness;
    }

    public Long getBrightness() {
        return brightness;
    }

    public void setTransitionTime(Long transitionTime) {
        this.transitionTime = transitionTime;
    }

    public Long getTransitionTime() {
        return transitionTime;
    }

}
