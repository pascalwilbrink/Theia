package nl.wilbrink.light.dto;

public class LightDTO {

    private String id;

    private String name;

    private LightStateDTO state;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setState(LightStateDTO state) {
        this.state = state;
    }

    public LightStateDTO getState() {
        return state;
    }
}
