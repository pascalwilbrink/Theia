package nl.wilbrink.light.audit;

import static nl.wilbrink.audit.entity.AuditEvent.Type.LIGHT_BRIGHTNESS_CHANGE;
import static nl.wilbrink.audit.entity.AuditEvent.Type.LIGHT_HUE_CHANGE;
import static nl.wilbrink.audit.entity.AuditEvent.Type.LIGHT_NAME_CHANGE;
import static nl.wilbrink.audit.entity.AuditEvent.Type.LIGHT_SATURATION_CHANGE;
import static nl.wilbrink.audit.entity.AuditEvent.Type.LIGHT_SWITCH_CHANGE;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import nl.wilbrink.audit.entity.AuditEvent;
import nl.wilbrink.audit.entity.AuditEvent.Type;
import nl.wilbrink.audit.service.AuditService;
import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.dto.LightStateDTO;
import nl.wilbrink.light.utils.LightUtils;

@Component
public class LightAuditProvider {

    private final AuditService auditService;
    private final LightUtils lightUtils;

    @Autowired
    public LightAuditProvider(AuditService auditService, LightUtils lightUtils) {
        this.auditService = auditService;
        this.lightUtils = lightUtils;
    }

    public void auditEvent(LightDTO oldLight, LightDTO newLight) {
        if (lightUtils.hasLightChange(oldLight, newLight)) {
            AuditEvent auditEvent = new AuditEvent();
            auditEvent.setType(getType(oldLight, newLight));
            auditEvent.setAuditTime(LocalDateTime.now());

            auditService.createEvent(auditEvent);
        }
    }

    public void auditEvent(LightStateDTO oldState, LightStateDTO newState) {
        if (lightUtils.hasStateChange(oldState, newState)) {

            AuditEvent auditEvent = new AuditEvent();
            auditEvent.setType(getType(oldState, newState));
            auditEvent.setAuditTime(LocalDateTime.now());

            auditService.createEvent(auditEvent);
        }
    }

    private Type getType(LightDTO oldLight, LightDTO newLight) {
        Type type = null;

        if (lightUtils.hasNameChange(oldLight, newLight)) type = LIGHT_NAME_CHANGE;

        if (type == null) {
            throw new RuntimeException();
        }

        return type;
    }

    private Type getType(LightStateDTO oldState, LightStateDTO newState) {
        Type type = null;

        if (lightUtils.hasOnChange(oldState, newState)) type = LIGHT_SWITCH_CHANGE;
        if (lightUtils.hasHueChange(oldState, newState)) type = LIGHT_HUE_CHANGE;
        if (lightUtils.hasSaturationChange(oldState, newState)) type = LIGHT_SATURATION_CHANGE;
        if (lightUtils.hasBrightnessChange(oldState, newState))  type = LIGHT_BRIGHTNESS_CHANGE;

        if (type == null) {
            throw new RuntimeException();
        }

        return type;
    }
}
