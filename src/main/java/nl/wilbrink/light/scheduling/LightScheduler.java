package nl.wilbrink.light.scheduling;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.service.LightManagementService;
import nl.wilbrink.light.service.UpdateLightService;
import nl.wilbrink.light.utils.LightUtils;

@Component
public class LightScheduler {

    private final LightManagementService lightManagementService;
    private final LightUtils lightUtils;
    private final UpdateLightService updateLightService;

    private Map<String, LightDTO> lights = new HashMap<>();

    @Autowired
    public LightScheduler(LightManagementService lightManagementService, LightUtils lightUtils, UpdateLightService updateLightService) {
        this.lightManagementService = lightManagementService;
        this.updateLightService = updateLightService;
        this.lightUtils = lightUtils;

    }

    @Scheduled(fixedRateString = "${schedule.light.update:5000}", initialDelayString = "${schedule.light.update:5000}")
    public void checkLights() {
        lightManagementService.getAllLights().forEach(light -> {
            if (!lights.containsKey(light.getId())) lights.put(light.getId(), light);

            if (lightUtils.hasStateChange(light.getState(), lights.get(light.getId()).getState())) {
                updateLight(light);
            }
        });
    }

    private void updateLight(LightDTO light) {
        updateLightService.handleLightIfChanged(lights.get(light.getId()), light);
        updateLightService.handleLightStateIfChanged(lights.get(light.getId()).getState(), light.getState(), light.getId());
        lights.put(light.getId(), light);
    }

}
