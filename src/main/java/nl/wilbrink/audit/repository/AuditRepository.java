package nl.wilbrink.audit.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import nl.wilbrink.audit.entity.AuditEvent;

@Repository
public interface AuditRepository extends CrudRepository<AuditEvent, String> {

}
