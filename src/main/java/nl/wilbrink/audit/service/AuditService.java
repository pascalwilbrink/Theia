package nl.wilbrink.audit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.wilbrink.audit.entity.AuditEvent;
import nl.wilbrink.audit.repository.AuditRepository;

@Service
public class AuditService {

    private final AuditRepository auditRepository;

    @Autowired
    public AuditService(AuditRepository auditRepository) {
        this.auditRepository = auditRepository;
    }

    public void createEvent(AuditEvent auditEvent) {
        auditRepository.save(auditEvent);
    }

}
