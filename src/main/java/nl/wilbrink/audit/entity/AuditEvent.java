package nl.wilbrink.audit.entity;

import nl.wilbrink.common.entity.BaseEntity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Table(name = "audit_events")
@Entity
public class AuditEvent extends BaseEntity {

    @Enumerated(EnumType.STRING)
    @Column
    private Type type;

    @Column(name = "audit_time")
    private LocalDateTime auditTime;

    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    public void setAuditTime(LocalDateTime auditTime) {
        this.auditTime = auditTime;
    }

    public LocalDateTime getAuditTime() {
        return auditTime;
    }

    public enum Type {
        LIGHT_NAME_CHANGE,
        LIGHT_SWITCH_CHANGE,
        LIGHT_HUE_CHANGE,
        LIGHT_SATURATION_CHANGE,
        LIGHT_BRIGHTNESS_CHANGE
    }
}
