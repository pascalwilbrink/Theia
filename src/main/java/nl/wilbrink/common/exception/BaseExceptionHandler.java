package nl.wilbrink.common.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static nl.wilbrink.common.exception.WebException.HttpStatus.BAD_REQUEST;

@ControllerAdvice
public class BaseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = Throwable.class)
    protected ResponseEntity<ExceptionMessage> handleThrowable(Throwable throwable, ServletWebRequest request) {
        ExceptionMessage message = new ExceptionMessage(BAD_REQUEST.name(), BAD_REQUEST.getStatusCode(), request.getRequest().getServletPath());

        return ResponseEntity.status(BAD_REQUEST.getStatusCode()).body(message);

    }
}
