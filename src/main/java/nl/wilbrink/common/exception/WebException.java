package nl.wilbrink.common.exception;

public class WebException extends RuntimeException {

    private HttpStatus status;

    public WebException(String message, HttpStatus status) {
        this(message, status, null);
    }

    public WebException(String message, HttpStatus status, Throwable cause) {
        super(message, cause);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public enum HttpStatus {
        NOT_FOUND(404),
        UNAUTHORIZED(401),
        BAD_REQUEST(400),
        INTERNAL_SERVER_ERROR(500)
        ;

        private int statusCode;

        HttpStatus(int statusCode) {
            this.statusCode = statusCode;
        }

        public int getStatusCode() {
            return statusCode;
        }
    }
}
