package nl.wilbrink.common.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class WebExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = WebException.class)
    protected ResponseEntity<ExceptionMessage> handleWebException(WebException exception, ServletWebRequest request) {
        ExceptionMessage message = new ExceptionMessage(exception, request.getRequest().getServletPath());

        return ResponseEntity.status(exception.getStatus().getStatusCode()).body(message);
    }

}
