package nl.wilbrink.common.exception;

public class ExceptionMessage {

    private String message;

    private String path;

    private int status;

    public ExceptionMessage(WebException cause, String path) {
        this.message = cause.getMessage();
        this.status = cause.getStatus().getStatusCode();
        this.path = path;
    }

    public ExceptionMessage(String message, int status, String path) {
        this.message = message;
        this.status = status;
        this.path = path;
    }

    public String getMessage() {
        return message;
    }

    public String getPath() {
        return path;
    }

    public int getStatus() {
        return status;
    }
}
