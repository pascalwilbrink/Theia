package nl.wilbrink.common.event;

public class BaseEvent<T> {

    private T source;

    public BaseEvent(T source) {
        this.source = source;
    }

    public T getSource() {
        return source;
    }
}
