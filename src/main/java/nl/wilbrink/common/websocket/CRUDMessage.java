package nl.wilbrink.common.websocket;

public class CRUDMessage extends WebSocketMessage<String> {

    private Method method;

    public CRUDMessage(String source, Method method) {
        super(source);
        this.method = method;
    }

    public Method getMethod() {
        return method;
    }

    public enum Method {
        CREATE,
        READ,
        UPDATE,
        DELETE
    }
}
