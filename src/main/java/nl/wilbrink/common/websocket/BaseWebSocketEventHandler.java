package nl.wilbrink.common.websocket;

public interface BaseWebSocketEventHandler<T extends WebSocketMessage> {

    void onMessage(T message);

}
