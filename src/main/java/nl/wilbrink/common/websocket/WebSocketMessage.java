package nl.wilbrink.common.websocket;

public class WebSocketMessage<T> {

    private T source;

    public WebSocketMessage(T source) {
        this.source = source;
    }

    public T getSource() {
        return source;
    }

}
