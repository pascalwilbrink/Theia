package nl.wilbrink.common.logging;

import com.rollbar.notifier.Rollbar;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class BaseLogger implements Ilogger {

    @Autowired
    private Rollbar rollbar;

    Logger logger = Logger.getGlobal();

    @Override
    public void debug(String message) {
        rollbar.debug(message);
        logger.log(Level.FINE, message);
    }

    @Override
    public void info(String message) {
        rollbar.info(message);
        logger.log(Level.INFO, message);
    }

    @Override
    public void warning(String message) {
        rollbar.warning(message);
        logger.log(Level.WARNING, message);
    }

    @Override
    public void error(String message, Throwable cause) {
        rollbar.error(cause, message);
        logger.log(Level.SEVERE, message, cause);
    }

    @Override
    public void critical(String message, Throwable cause) {
        rollbar.critical(cause, message);
        logger.log(Level.SEVERE, message, cause);
    }
}
