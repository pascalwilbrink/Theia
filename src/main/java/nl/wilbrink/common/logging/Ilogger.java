package nl.wilbrink.common.logging;

public interface Ilogger {

    void debug(String message);

    void info(String message);

    void warning(String message);

    void error(String message, Throwable cause);

    void critical(String message, Throwable cause);

}
