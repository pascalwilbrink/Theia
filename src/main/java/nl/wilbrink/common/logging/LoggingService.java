package nl.wilbrink.common.logging;

import nl.wilbrink.common.exception.WebException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class LoggingService {

    @Autowired
    private Ilogger logger;

    public void debug(String message) {
        logger.debug(log(LogLevel.DEBUG, message));
    }

    public void info(String message) {
        logger.info(log(LogLevel.INFO, message));
    }

    public void warning(String message) {
        logger.warning(log(LogLevel.WARNING, message));
    }

    public void error(String message) {
        error(message, null);
    }

    public WebException error(String message, WebException cause) {
        logger.error(log(LogLevel.ERROR, message), cause);
        return cause;
    }

    public void critical(String message) {
        critical(message, null);
    }

    public WebException critical(String message, WebException cause) {
        logger.critical(log(LogLevel.CRITICAL, message), cause);
        return cause;
    }

    private String log(LogLevel logLevel, String message) {
        return String.format( "%s: %s [%s]", logLevel.name(), message, LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME));
    }

    private enum LogLevel {
        DEBUG,
        INFO,
        WARNING,
        ERROR,
        CRITICAL
    }

}
