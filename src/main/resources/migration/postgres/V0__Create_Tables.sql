create table config_items (
    id varchar(36) NOT NULL PRIMARY KEY,
    config_key varchar(200) NOT NULL UNIQUE,
    config_value varchar(1000),
    created timestamp NOT NULL,
    created_by varchar(36),
    updated timestamp,
    updated_by varchar(36)
);

create table lights (
    id varchar(36) NOT NULL PRIMARY KEY,
    external_id varchar(100) UNIQUE,
    created timestamp NOT NULL,
    created_by varchar(36),
    updated timestamp,
    updated_by varchar(36)
);

create table audit_events (
    id varchar(36) NOT NULL PRIMARY KEY,
    type varchar(36) NOT NULL,
    audit_time timestamp NOT NULL,
    created timestamp NOT NULL,
    created_by varchar(36),
    updated timestamp,
    updated_by varchar(36)
);

insert into config_items (id, config_key, config_value, created)
    values('65e6da37-85f9-4372-b40c-c7c74a6dec74', 'LIGHT_TYPE', 'HUE', now());