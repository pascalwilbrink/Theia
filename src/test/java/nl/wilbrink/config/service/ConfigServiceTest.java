package nl.wilbrink.config.service;

import nl.wilbrink.config.ConfigItemDTOBuilder;
import nl.wilbrink.config.ConfigItemKey;
import nl.wilbrink.config.dto.ConfigItemDTO;
import nl.wilbrink.config.entity.ConfigItem;
import nl.wilbrink.config.mapper.ConfigItemMapper;
import nl.wilbrink.config.repository.ConfigItemRepository;
import nl.wilbrink.light.LightType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfigServiceTest {

    @Mock
    private ConfigItemRepository configItemRepository;

    @Mock
    private ConfigItemMapper configItemMapper;

    @InjectMocks
    private ConfigService subject;

    @Test
    public void itShouldGetAllConfigItems() {
        ConfigItemDTO configItem1 = ConfigItemDTOBuilder
                .aConfigItemDTO()
                .withId(java.util.UUID.randomUUID().toString())
                .withKey(ConfigItemKey.LIGHT_TYPE)
                .withValue(LightType.HUE.name())
                .build();

        ConfigItem entity1 = new ConfigItem();
        when(configItemMapper.toDTO(entity1)).thenReturn(configItem1);

        when(configItemRepository.findAll()).thenReturn(Arrays.asList(entity1));

        List<ConfigItemDTO> result = subject.getAllConfigItems();

        assertThat(result, containsInAnyOrder(configItem1));
    }

    @Test
    public void itShouldGetAConfigItemById() {
        String id = java.util.UUID.randomUUID().toString();

        ConfigItem entity = new ConfigItem();
        ConfigItemDTO dto = new ConfigItemDTO();

        when(configItemRepository.findOne(id)).thenReturn(entity);
        when(configItemMapper.toDTO(entity)).thenReturn(dto);

        ConfigItemDTO result = subject.getConfigItemById(id);

        assertThat(result, equalTo(dto));
    }

    @Test
    public void itShouldGetAConfigItemByKey() {
        ConfigItem entity = new ConfigItem();
        ConfigItemDTO dto = new ConfigItemDTO();

        when(configItemRepository.findOneByKey("LIGHT_TYPE")).thenReturn(entity);
        when(configItemMapper.toDTO(entity)).thenReturn(dto);

        ConfigItemDTO result = subject.getConfigItemByKey(ConfigItemKey.LIGHT_TYPE);

        assertThat(result, equalTo(dto));
    }

    @Test
    public void itShouldUpdateAConfigItem() {
        String id = java.util.UUID.randomUUID().toString();
        ConfigItem entity = new ConfigItem();
        entity.setId(id);
        ConfigItemDTO dto = new ConfigItemDTO();
        dto.setId(id);

        when(configItemMapper.toEntity(dto)).thenReturn(entity);
        when(configItemMapper.toDTO(entity)).thenReturn(dto);

        when(configItemRepository.save(entity)).thenReturn(entity);

        ConfigItemDTO result = subject.updateConfigItem(dto);

        assertThat(result, equalTo(dto));
    }

}