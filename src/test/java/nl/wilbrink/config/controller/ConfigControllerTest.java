package nl.wilbrink.config.controller;

import nl.wilbrink.config.ConfigItemDTOBuilder;
import nl.wilbrink.config.ConfigItemKey;
import nl.wilbrink.config.dto.ConfigItemDTO;
import nl.wilbrink.config.service.ConfigService;
import nl.wilbrink.light.LightType;
import nl.wilbrink.light.dto.LightDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfigControllerTest {

    @Mock
    private ConfigService configService;

    @InjectMocks
    private ConfigController subject;

    @Test
    public void itShouldGetAllConfigItems() {
        ConfigItemDTO configItem1 = ConfigItemDTOBuilder
                .aConfigItemDTO()
                .withId(java.util.UUID.randomUUID().toString())
                .withKey(ConfigItemKey.LIGHT_TYPE)
                .withValue(LightType.HUE.name())
                .build();

        when(configService.getAllConfigItems()).thenReturn(Arrays.asList(configItem1));

        ResponseEntity response = subject.getAllConfigItems();

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), notNullValue());
        assertThat(response.getBody(), instanceOf(List.class));

        List<ConfigItemDTO> configItems = (List)response.getBody();
        assertThat(configItems, containsInAnyOrder(configItem1));
    }

    @Test
    public void itShouldGetAConfigItemById() {
        String id = java.util.UUID.randomUUID().toString();

        ConfigItemDTO configItem = ConfigItemDTOBuilder
                .aConfigItemDTO()
                .withId(id)
                .withKey(ConfigItemKey.LIGHT_TYPE)
                .withValue("HUE")
                .build();

        when(configService.getConfigItemById(id)).thenReturn(configItem);

        ResponseEntity response = subject.getConfigItem(id);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), instanceOf(ConfigItemDTO.class));
        assertThat(response.getBody(), equalTo(configItem));
    }

    @Test
    public void itShouldUpdateAConfigItem() {
        String id = java.util.UUID.randomUUID().toString();

        ConfigItemDTO configItem = ConfigItemDTOBuilder
                .aConfigItemDTO()
                .withKey(ConfigItemKey.LIGHT_TYPE)
                .withValue(LightType.HUE.name())
                .build();

        when(configService.updateConfigItem(configItem)).thenReturn(configItem);

        ResponseEntity response = subject.updateConfigItem(id, configItem);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), instanceOf(ConfigItemDTO.class));
        assertThat(response.getBody(), equalTo(configItem));
        assertThat(((ConfigItemDTO)response.getBody()).getId(), equalTo(id));
    }
}