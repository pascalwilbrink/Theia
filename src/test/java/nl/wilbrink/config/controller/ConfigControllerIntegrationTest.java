package nl.wilbrink.config.controller;

import nl.wilbrink.BaseIntegrationTest;
import nl.wilbrink.config.dto.ConfigItemDTO;
import nl.wilbrink.light.LightType;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasSize;

public class ConfigControllerIntegrationTest extends BaseIntegrationTest {

    @Autowired
    private ConfigController configController;

    @Test
    public void itShouldGetAllConfigItems() {
        ResponseEntity<List<ConfigItemDTO>> configItems = configController.getAllConfigItems();

        assertThat(configItems.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(configItems.getBody(), hasSize(1));

        assertThat(configItems.getBody().get(0).getPossibleValues(), containsInAnyOrder(LightType.HUE.name(), LightType.NONE.name(), LightType.LIFX.name()));
    }
}
