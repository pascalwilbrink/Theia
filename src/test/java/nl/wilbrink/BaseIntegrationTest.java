package nl.wilbrink;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.NONE;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = { Application.class },
        webEnvironment = NONE
)
public abstract class BaseIntegrationTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(1111);

}
