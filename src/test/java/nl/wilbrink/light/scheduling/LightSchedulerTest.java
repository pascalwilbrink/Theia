package nl.wilbrink.light.scheduling;

import nl.wilbrink.light.LightDTOBuilder;
import nl.wilbrink.light.LightStateDTOBuilder;
import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.service.LightManagementService;
import nl.wilbrink.light.service.UpdateLightService;
import nl.wilbrink.light.utils.LightUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LightSchedulerTest {

    @Mock
    private LightManagementService lightManagementService;

    @Mock
    private LightUtils lightUtils;

    @Mock
    private UpdateLightService updateLightService;

    private LightScheduler subject;

    private LightDTO light1, light2, light3;

    @Before
    public void initLights() {
        light1 = LightDTOBuilder
                .aLightDTO()
                .withName("Light 1")
                .withId(java.util.UUID.randomUUID().toString())
                .withState(LightStateDTOBuilder
                    .aLightStateDTO()
                    .withHue(5000L)
                    .withSaturation(255L)
                    .withBrightness(255L)
                    .withOn(true)
                    .build()
                )
                .build();

        light2 = LightDTOBuilder
                .aLightDTO()
                .withName("Light 2")
                .withId(java.util.UUID.randomUUID().toString())
                .withState(LightStateDTOBuilder
                        .aLightStateDTO()
                        .withHue(15000L)
                        .withSaturation(255L)
                        .withBrightness(255L)
                        .withOn(false)
                        .build()
                )
                .build();

        light3 = LightDTOBuilder
                .aLightDTO()
                .withName("Light 3")
                .withId(java.util.UUID.randomUUID().toString())
                .withState(LightStateDTOBuilder
                        .aLightStateDTO()
                        .withHue(10000L)
                        .withSaturation(100L)
                        .withBrightness(255L)
                        .withOn(true)
                        .build()
                )
                .build();

        when(lightManagementService.getAllLights()).thenReturn(Arrays.asList(light1, light2, light3));

        subject = new LightScheduler(lightManagementService, lightUtils, updateLightService);
    }

    @Test
    public void itShouldFindADifferenceInLight1() {
        LightDTO light1Difference = LightDTOBuilder
                .aLightDTO()
                .withName(light1.getName())
                .withId(light1.getId())
                .withState(
                    LightStateDTOBuilder
                        .aLightStateDTO()
                        .withHue(15000L)
                        .withSaturation(light1.getState().getSaturation())
                        .withBrightness(light1.getState().getBrightness())
                        .withOn(light1.getState().getOn())
                        .build()
                )
                .build();

        when(lightManagementService.getAllLights()).thenReturn(Arrays.asList(light1Difference, light2, light3));

        subject.checkLights();
    }

    @Test
    public void itShouldAddANewLight() {
        subject.checkLights();

        LightDTO light4 = LightDTOBuilder
            .aLightDTO()
            .withName("Light 4")
            .withId(java.util.UUID.randomUUID().toString())
            .withState(
                LightStateDTOBuilder
                    .aLightStateDTO()
                    .withHue(15000L)
                    .withSaturation(200L)
                    .withBrightness(200L)
                    .withOn(false)
                    .build()
            )
            .build();

        when(lightManagementService.getAllLights()).thenReturn(Arrays.asList(light1, light2, light3, light4));

        subject.checkLights();
    }
}

