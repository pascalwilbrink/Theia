package nl.wilbrink.light.controller;

import nl.wilbrink.config.dto.ConfigItemDTO;
import nl.wilbrink.light.LightDTOBuilder;
import nl.wilbrink.light.LightStateDTOBuilder;
import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.dto.LightStateDTO;
import nl.wilbrink.light.service.LightManagementService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LightControllerTest {

    @Mock
    private LightManagementService lightManagementService;

    @InjectMocks
    private LightController subject;

    @Test
    public void itShouldGetAllLights() {
        LightDTO light1 = LightDTOBuilder
                .aLightDTO()
                .withId(java.util.UUID.randomUUID().toString())
                .withName("Light 1")
                .build();

        LightDTO light2 = LightDTOBuilder
                .aLightDTO()
                .withId(java.util.UUID.randomUUID().toString())
                .withName("Light 2")
                .build();

        when(lightManagementService.getAllLights()).thenReturn(Arrays.asList(light1, light2));

        ResponseEntity response = subject.getAllLights();

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), notNullValue());
        assertThat(response.getBody(), instanceOf(List.class));

        List<LightDTO> lights = (List)response.getBody();
        assertThat(lights, containsInAnyOrder(light1, light2));
    }

    @Test
    public void itShouldGetALightById() {
        String id = java.util.UUID.randomUUID().toString();

        LightDTO light = LightDTOBuilder
                .aLightDTO()
                .withId(id)
                .withName("Light 1")
                .build();

        when(lightManagementService.getLight(id)).thenReturn(light);

        ResponseEntity<LightDTO> response = subject.getLight(id);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), equalTo(light));
    }

    @Test
    public void itShouldUpdateALight() {
        String id = java.util.UUID.randomUUID().toString();

        LightDTO light = LightDTOBuilder
                .aLightDTO()
                .withName("Light 1")
                .build();

        when(lightManagementService.updateLight(light)).thenReturn(light);

        ResponseEntity<LightDTO> response = subject.updateLight(id, light);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), equalTo(light));
    }

    @Test
    public void itshouldUpdateALightState() {
        String id = java.util.UUID.randomUUID().toString();

        LightStateDTO state = LightStateDTOBuilder
                .aLightStateDTO()
                .withHue(10000L)
                .withSaturation(255L)
                .withBrightness(255L)
                .withOn(true)
                .withReachable(true)
                .withTransitionTime(1500L)
                .build();

        when(lightManagementService.updateLightState(id, state)).thenReturn(state);

        ResponseEntity<LightStateDTO> response = subject.updateLightState(id, state);

        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), equalTo(state));
    }
}