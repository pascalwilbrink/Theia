package nl.wilbrink.light.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.WireMock;
import nl.wilbrink.BaseIntegrationTest;
import nl.wilbrink.light.LightService;
import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.impl.hue.HueLightService;
import nl.wilbrink.light.impl.hue.LightResponse;
import nl.wilbrink.light.impl.hue.LightsResponse;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.assertj.core.api.Assertions.assertThat;

public class LightControllerIntegrationTest extends BaseIntegrationTest {

    @Autowired
    private LightController lightController;
    @Autowired
    private ObjectMapper objectMapper;

    private LightsResponse lightsResponse;

    private String lightId1, lightId2;

    @Before
    public void setupHueStub() throws JsonProcessingException {
        lightsResponse = mockLightsResponse();
        stubFor(WireMock.get(urlEqualTo("/api/key/lights")).willReturn(
                aResponse()
                .withHeader("Content-Type", "application/json")
                .withBody(objectMapper.writeValueAsString(lightsResponse))
        ));
    }


    public LightsResponse mockLightsResponse() {
        LightsResponse lightsResponse = new LightsResponse();

        lightId1 = java.util.UUID.randomUUID().toString();
        lightId2 = java.util.UUID.randomUUID().toString();

        lightsResponse.setLight(lightId1, createLightResponse("Light 1", 10000, 255, 255, true));
        lightsResponse.setLight(lightId2, createLightResponse("Light 2", 5000, 100, 100, false));

        return lightsResponse;
    }

    private LightResponse createLightResponse(String name, int hue, int saturation, int brightness, boolean on) {
        LightResponse lightResponse = new LightResponse();
        lightResponse.setName(name);
        lightResponse.setManufacturerName("Philips");
        lightResponse.setModelId("Model 1");
        lightResponse.setSwVersion("1.0");
        lightResponse.setUniqueId(java.util.UUID.randomUUID().toString());
        lightResponse.setType("Color");

        LightResponse.Capabilities capabilities = new LightResponse.Capabilities();
        LightResponse.Capabilities.Streaming streaming = new LightResponse.Capabilities.Streaming();
        streaming.setProxy(true);
        streaming.setRenderer(true);

        capabilities.setStreaming(streaming);

        lightResponse.setCapabilities(capabilities);

        LightResponse.SwUpdate swUpdate1 = new LightResponse.SwUpdate();
        swUpdate1.setState("Up to date");
        swUpdate1.setLastInstall("Yesterday");

        lightResponse.setSwUpdate(swUpdate1);

        LightResponse.State state = new LightResponse.State();
        state.setHue(hue);
        state.setSaturation(saturation);
        state.setBrightness(brightness);
        state.setOn(on);
        state.setAlert("None");
        state.setColorMode("Color");
        state.setEffect("None");
        state.setMode("None");
        state.setReachable(true);
        state.setXy(new float[]{ .5f, .4f});
        state.setColorTemperature("temperature");

        lightResponse.setState(state);

        return lightResponse;
    }

    @Test
    public void itShouldGetAllLights() {
        ResponseEntity<List<LightDTO>> response = lightController.getAllLights();

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).extracting(light -> light.getName()).containsExactlyInAnyOrder(
            "Light 1", "Light 2"
        );
    }

}
