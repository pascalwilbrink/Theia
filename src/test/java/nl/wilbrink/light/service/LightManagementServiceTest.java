package nl.wilbrink.light.service;

import nl.wilbrink.config.ConfigItemKey;
import nl.wilbrink.config.dto.ConfigItemDTO;
import nl.wilbrink.config.service.ConfigService;
import nl.wilbrink.light.*;
import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.dto.LightStateDTO;
import nl.wilbrink.light.utils.LightUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LightManagementServiceTest {

    @Mock
    private LightProviderFactory lightProviderFactory;
    @Mock
    private ConfigService configService;
    @Mock
    private LightService mockedLightService;
    @Mock
    private LightUtils lightUtils;
    @Mock
    private UpdateLightService updateLightService;

    @InjectMocks
    private LightManagementService subject;

    @Before
    public void setupConfig() {
        ConfigItemDTO configItem = new ConfigItemDTO();
        configItem.setValue(LightType.HUE.name());

        when(configService.getConfigItemByKey(ConfigItemKey.LIGHT_TYPE)).thenReturn(configItem);
        when(lightProviderFactory.getService(LightType.HUE)).thenReturn(mockedLightService);
    }

    @Test
    public void itShouldGetAllLights() {
        LightDTO light1 = LightDTOBuilder
                .aLightDTO()
                .withId(java.util.UUID.randomUUID().toString())
                .withName("Light 1")
                .build();

        LightDTO light2 = LightDTOBuilder
                .aLightDTO()
                .withId(java.util.UUID.randomUUID().toString())
                .withName("Light 2")
                .build();

        when(mockedLightService.getAllLights()).thenReturn(Arrays.asList(light1, light2));

        List<LightDTO> result = subject.getAllLights();

        assertThat(result, containsInAnyOrder(light1, light2));
    }

    @Test
    public void itShouldGetALightById() {
        String id = java.util.UUID.randomUUID().toString();

        LightDTO light = LightDTOBuilder
                .aLightDTO()
                .withId(id)
                .withName("Light 1")
                .build();

        when(mockedLightService.getLight(id)).thenReturn(light);

        LightDTO result = subject.getLight(id);

        assertThat(result, equalTo(light));
    }

    @Test
    public void itShouldUpdateALight() {
        String id = java.util.UUID.randomUUID().toString();

        LightDTO light = LightDTOBuilder
                .aLightDTO()
                .withId(id)
                .withName("Light 1")
                .build();

        LightDTO updatedLight = LightDTOBuilder
                .aLightDTO()
                .withId(id)
                .withName("Updated name")
                .build();
        when(mockedLightService.getLight(id)).thenReturn(light);
        when(lightUtils.hasLightChange(light, updatedLight)).thenReturn(true);
        when(mockedLightService.updateLight(updatedLight)).thenReturn(updatedLight);

        LightDTO result = subject.updateLight(updatedLight);

        assertThat(result, equalTo(updatedLight));
    }

    @Test
    public void itShouldNotUpdateALightStateWhenStateIsTheSame() {
        String id = java.util.UUID.randomUUID().toString();

        LightStateDTO state = LightStateDTOBuilder
            .aLightStateDTO()
            .withHue(10000L)
            .withSaturation(255L)
            .withBrightness(255L)
            .withOn(true)
            .withReachable(true)
            .withTransitionTime(1500L)
            .build();

        when(mockedLightService.updateLightState(id, state)).thenReturn(state);

        LightDTO light = mock(LightDTO.class);
        when(light.getState()).thenReturn(state);

        when(mockedLightService.getLight(id)).thenReturn(light);

        LightStateDTO result = subject.updateLightState(id, state);

        assertThat(result, equalTo(state));
    }


    @Test
    public void itShouldUpdateALightState() {
        String id = java.util.UUID.randomUUID().toString();

        LightStateDTO state = LightStateDTOBuilder
                .aLightStateDTO()
                .withHue(10000L)
                .withSaturation(255L)
                .withBrightness(255L)
                .withOn(true)
                .withReachable(true)
                .withTransitionTime(1500L)
                .build();

        LightStateDTO oldState = LightStateDTOBuilder
                .aLightStateDTO()
                .withHue(5000L)
                .withSaturation(255L)
                .withBrightness(255L)
                .withOn(true)
                .withReachable(true)
                .withTransitionTime(1500L)
                .build();

        when(mockedLightService.updateLightState(id, state)).thenReturn(state);

        LightDTO light = mock(LightDTO.class);
        when(light.getState()).thenReturn(oldState);

        when(mockedLightService.getLight(id)).thenReturn(light);

        LightStateDTO result = subject.updateLightState(id, state);

        assertThat(result, equalTo(state));
    }

}