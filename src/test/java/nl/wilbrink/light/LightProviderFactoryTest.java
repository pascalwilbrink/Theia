package nl.wilbrink.light;

import nl.wilbrink.common.logging.LoggingService;
import nl.wilbrink.light.impl.dummy.DummyLightService;
import nl.wilbrink.light.impl.hue.HueLightService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Arrays.asList;
import static nl.wilbrink.light.LightType.HUE;
import static nl.wilbrink.light.LightType.NONE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LightProviderFactoryTest {

    @Mock
    private HueLightService hueLightService;
    @Mock
    private DummyLightService dummyLightService;
    @Mock
    private LoggingService loggingService;

    private LightProviderFactory subject;

    @Before
    public void initLightProvider() {
        when(hueLightService.getLightType()).thenReturn(HUE);
        when(dummyLightService.getLightType()).thenReturn(NONE);
        subject = new LightProviderFactory(asList(hueLightService), loggingService);
    }

    @Test
    public void itShouldGetHueServiceByType() {
        LightService result = subject.getService(HUE);

        assertThat(result, equalTo(hueLightService));
    }
}