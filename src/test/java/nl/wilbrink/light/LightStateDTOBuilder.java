package nl.wilbrink.light;

import nl.wilbrink.light.dto.LightStateDTO;

public final class LightStateDTOBuilder {
    private boolean on;
    private boolean reachable;
    private Long hue;
    private Long saturation;
    private Long brightness;
    private Long transitionTime;

    private LightStateDTOBuilder() {
    }

    public static LightStateDTOBuilder aLightStateDTO() {
        return new LightStateDTOBuilder();
    }

    public LightStateDTOBuilder withOn(boolean on) {
        this.on = on;
        return this;
    }

    public LightStateDTOBuilder withReachable(boolean reachable) {
        this.reachable = reachable;
        return this;
    }

    public LightStateDTOBuilder withHue(Long hue) {
        this.hue = hue;
        return this;
    }

    public LightStateDTOBuilder withSaturation(Long saturation) {
        this.saturation = saturation;
        return this;
    }

    public LightStateDTOBuilder withBrightness(Long brightness) {
        this.brightness = brightness;
        return this;
    }

    public LightStateDTOBuilder withTransitionTime(Long transitionTime) {
        this.transitionTime = transitionTime;
        return this;
    }

    public LightStateDTO build() {
        LightStateDTO lightStateDTO = new LightStateDTO();
        lightStateDTO.setOn(on);
        lightStateDTO.setReachable(reachable);
        lightStateDTO.setHue(hue);
        lightStateDTO.setSaturation(saturation);
        lightStateDTO.setBrightness(brightness);
        lightStateDTO.setTransitionTime(transitionTime);
        return lightStateDTO;
    }
}
