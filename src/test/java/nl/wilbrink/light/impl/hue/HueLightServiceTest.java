package nl.wilbrink.light.impl.hue;

import nl.wilbrink.common.exception.WebException;
import nl.wilbrink.common.logging.LoggingService;
import nl.wilbrink.light.LightStateDTOBuilder;
import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.dto.LightStateDTO;
import nl.wilbrink.light.entity.Light;
import nl.wilbrink.light.repository.LightRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static nl.wilbrink.common.exception.WebException.HttpStatus.NOT_FOUND;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HueLightServiceTest {

    @Mock
    private LightRepository lightRepository;

    @Mock
    private HueLightService.HueAPIService hueAPIService;

    @Mock
    private HueLightMapper lightMapper;

    @Mock
    private LoggingService loggingService;

    @InjectMocks
    private HueLightService subject;

    @Mock
    private LightsResponse lightsResponse;
    @Mock
    private LightResponse lightResponse1, lightResponse2;
    @Mock
    private Light entity1, entity2;
    @Mock
    private LightDTO light1, light2;

    private String lightId1 = java.util.UUID.randomUUID().toString();
    private String lightId2 = java.util.UUID.randomUUID().toString();
    @Before
    public void initLightsResponse() {
        Map<String, LightResponse> lights = new HashMap<>();
        lights.put(lightId1, lightResponse1);
        lights.put(lightId2, lightResponse2);

        when(lightsResponse.getLights()).thenReturn(lights);

        when(lightRepository.findOneByExternalId(lightId1)).thenReturn(entity1);
        when(lightRepository.findOneByExternalId(lightId2)).thenReturn(entity2);

        when(lightMapper.toDTO(lightResponse1)).thenReturn(light1);
        when(lightMapper.toDTO(lightResponse2)).thenReturn(light2);

        when(hueAPIService.getLights()).thenReturn(lightsResponse);

        when(entity1.getExternalId()).thenReturn(lightId1);
        when(entity2.getExternalId()).thenReturn(lightId2);
    }

    @Test
    public void itShouldGetAllLightsFromHue() {
        List<LightDTO> result = subject.getAllLights();

        assertThat(result, containsInAnyOrder(light1, light2));
    }

    @Test
    public void itShouldSaveANewLightWhenHueReturnsANewLight() {
        String id = java.util.UUID.randomUUID().toString();

        LightResponse lightResponse = mock(LightResponse.class);

        Map<String, LightResponse> lights = new HashMap<>();
        lights.put(lightId1, lightResponse1);
        lights.put(lightId2, lightResponse2);
        lights.put(id, lightResponse);

        Light entity = mock(Light.class);
        LightDTO light = mock(LightDTO.class);

        when(lightMapper.toDTO(lightResponse)).thenReturn(light);
        when(lightRepository.save(any(Light.class))).thenReturn(entity);

        when(lightsResponse.getLights()).thenReturn(lights);

        List<LightDTO> result = subject.getAllLights();

        assertThat(result, containsInAnyOrder(light1, light2, light));
    }

    @Test
    public void itShouldGetALightFromHue() {
        String id = java.util.UUID.randomUUID().toString();

        when(hueAPIService.getLight(lightId1)).thenReturn(lightResponse1);
        when(lightRepository.findOne(id)).thenReturn(entity1);

        LightDTO result = subject.getLight(id);

        assertThat(result, equalTo(light1));
    }

    @Test
    public void itShouldthrowAnExceptionWhenLightIsNotFound() {
        String id = java.util.UUID.randomUUID().toString();

        assertThatExceptionOfType(WebException.class).isThrownBy(() -> {
            when(loggingService.error(any(), any())).thenReturn(new WebException("Light not found", NOT_FOUND));

            subject.getLight(id);
        }).withMessage(String.format("Light not found", id));
    }

    @Test
    public void itShouldUpdateALightState() {
        String id = java.util.UUID.randomUUID().toString();

        LightStateDTO state = LightStateDTOBuilder
                .aLightStateDTO()
                .withOn(true)
                .withBrightness(255L)
                .withSaturation(255L)
                .withHue(1000L)
                .withTransitionTime(1500L)
                .build();

        when(lightRepository.findOne(id)).thenReturn(entity1);
        when(light1.getState()).thenReturn(state);
        when(hueAPIService.getLight(lightId1)).thenReturn(lightResponse1);
        LightStateDTO result = subject.updateLightState(id, state);

        assertThat(result, equalTo(state));
    }
}


