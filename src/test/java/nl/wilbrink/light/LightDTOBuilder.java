package nl.wilbrink.light;

import nl.wilbrink.light.dto.LightDTO;
import nl.wilbrink.light.dto.LightStateDTO;

public final class LightDTOBuilder {
    private String id;
    private String name;
    private LightStateDTO state;

    private LightDTOBuilder() {
    }

    public static LightDTOBuilder aLightDTO() {
        return new LightDTOBuilder();
    }

    public LightDTOBuilder withId(String id) {
        this.id = id;
        return this;
    }

    public LightDTOBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public LightDTOBuilder withState(LightStateDTO state) {
        this.state = state;
        return this;
    }

    public LightDTO build() {
        LightDTO lightDTO = new LightDTO();
        lightDTO.setId(id);
        lightDTO.setName(name);
        lightDTO.setState(state);
        return lightDTO;
    }
}
