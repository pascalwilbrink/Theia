package nl.wilbrink.light.utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import nl.wilbrink.light.LightStateDTOBuilder;
import nl.wilbrink.light.dto.LightStateDTO;

@RunWith(MockitoJUnitRunner.class)
public class LightUtilsTest {

    @InjectMocks
    private LightUtils subject;

    private LightStateDTO oldState, newState;

    @Before
    public void initStates() {
        oldState = LightStateDTOBuilder
            .aLightStateDTO()
            .withOn(true)
            .withReachable(true)
            .withHue(10000L)
            .withSaturation(255L)
            .withBrightness(255L)
            .build();

        newState = LightStateDTOBuilder
            .aLightStateDTO()
            .withOn(true)
            .withReachable(true)
            .withHue(10000L)
            .withSaturation(255L)
            .withBrightness(255L)
            .build();
    }

    @Test
    public void givenTwoStatesWhenStatesAreTheSameThenNoChangeShouldBeFound() {
        assertFalse(subject.hasStateChange(oldState, oldState));
    }

    @Test
    public void givenTwoStatesWhenTheStatesAreDifferentThenChangeShouldBeFound() {
        newState.setHue(5000L);

        assertTrue(subject.hasStateChange(oldState, newState));
    }

    @Test
    public void givenTwoStatesWhenThereIsNoDifferenceThenNoChangeShouldBeFound() {
        assertFalse(subject.hasStateChange(oldState, newState));
    }

    @Test
    public void givenTwoStatesWhenOnStateIsDifferentThenChangeShouldBeFound() {
        newState.setOn(!newState.getOn());
        assertTrue(subject.hasOnChange(oldState, newState));
    }

    @Test
    public void givenTwoStatesWhenReachableStateIsDifferentThenChangeShouldBeFound() {
        newState.setReachable(!newState.getReachable());
        assertTrue(subject.hasReachableChange(oldState, newState));
    }

    @Test
    public void givenTwoStatesWhenHueStateIsDifferentThenChangeShouldBeFound() {
        newState.setHue(5000L);
        assertTrue(subject.hasHueChange(oldState, newState));
    }

    @Test
    public void givenTwoStatesWhenSaturationStateIsDifferentThenChangeShouldBeFound() {
        newState.setSaturation(100L);
        assertTrue(subject.hasSaturationChange(oldState, newState));
    }

    @Test
    public void givenTwoStatesWhenBrightnessStateIsDifferentThenChangeShouldBeFound() {
        newState.setBrightness(100L);
        assertTrue(subject.hasBrightnessChange(oldState, newState));
    }

}